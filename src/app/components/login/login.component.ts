import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private route: Router,
    private auth: AuthenticationService
  ) {
  }

  username = '';
  password = '';
  errorMessage = 'Invalid credentials';
  invalidLogin = false;

  ngOnInit() {
  }

  basicAuthLogin() {
    this.auth.auth(this.username, this.password).subscribe(
      data => {
        this.route.navigate(['welcome', this.username]);
        this.invalidLogin = false;
      },
      error => {
        this.invalidLogin = true;
      }
    );
  }
}
