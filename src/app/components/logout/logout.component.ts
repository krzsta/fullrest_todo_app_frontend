import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(
    private auth: AuthenticationService,
    private route: Router
  ) {
  }

  ngOnInit() {
    this.auth.logoutUser();
    return this.route.navigate(['/']);
  }

}
