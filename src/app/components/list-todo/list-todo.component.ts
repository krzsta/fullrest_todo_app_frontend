import {Component, OnInit} from '@angular/core';
import {TodoDataService} from '../../services/todoData/todo-data.service';
import {Router} from '@angular/router';

export class Todo {
  constructor(
    public id: number,
    public desc: string,
    public done: boolean,
    public targetDate: Date
  ) {

  }
}

@Component({
  selector: 'app-list-todo',
  templateUrl: './list-todo.component.html',
  styleUrls: ['./list-todo.component.css']
})
export class ListTodoComponent implements OnInit {
  private successMessage: string;

  constructor(private todoDataService: TodoDataService,
              private router: Router) {
  }

  todos: Todo[];
  deleteMessage: string;

  ngOnInit() {
    this.retrieveAllTodo();
  }

  retrieveAllTodo() {
    this.todoDataService.retrieveAllTodo('user').subscribe(
      response => {
        this.todos = response;
      }
    );
  }

  deleteTodo(id: number) {
    this.todoDataService.deleteTodoById('user', id).subscribe(
      response => {
        this.retrieveAllTodo();
        this.deleteMessage = `Delete Successful`;
      }
    );
  }

  updateTodo(id: number) {
    this.router.navigate(['todos', id]);
  }

  addTodo() {
    this.router.navigate(['todos', -1]);
  }
}
