import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {WelcomeDataService} from '../../services/welcomeData/welcome-data.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private welcomeData: WelcomeDataService) {
  }

  name = '';
  messageFromRest: string;

  ngOnInit() {
    this.name = this.route.snapshot.params.name;
  }

  handleResponse(response) {
    this.messageFromRest = response.message;
  }
}
