import {Component, OnInit} from '@angular/core';
import {TodoDataService} from '../../services/todoData/todo-data.service';
import {Todo} from '../list-todo/list-todo.component';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  private id: number;
  private todo: Todo;

  constructor(
    private todoDataService: TodoDataService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.todo = new Todo(this.id, '', false, new Date());

    if (this.id != -1) {
      this.todoDataService.retrieveTodoById('user', this.id).subscribe(
        data => {
          this.todo = data;
        }
      );
    } else {
    }
  }

  saveTodo() {
    if (this.id === -1) {
    } else {
      this.todoDataService.updateTodo('user', this.id, this.todo).subscribe(
        data => {
          this.router.navigate(['/todos']);
        }
      );
    }
  }
}
