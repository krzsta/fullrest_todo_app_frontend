import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {API_URL} from '../../app.constans';

export const TOKEN = 'token';
export const AUTH_USER = 'userAuth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) {
  }

  isUserLogged() {
    const user = sessionStorage.getItem(AUTH_USER);
    return !(user === null);
  }

  logoutUser() {
    sessionStorage.removeItem(AUTH_USER);
    sessionStorage.removeItem(TOKEN);
  }

  auth(username: string, pass: string) {
    return this.http.post<any>(`${API_URL}/authenticate`, {username, password: pass})
      .pipe(map(data => {
        sessionStorage.setItem(AUTH_USER, username);
        sessionStorage.setItem(TOKEN, `Bearer ${data.token}`);
        return data;
      }));
  }

  getAuthUser() {
    return sessionStorage.getItem(AUTH_USER);
  }

  getAuthToken() {
    if (this.getAuthUser()) {
      return sessionStorage.getItem(TOKEN);
    }
  }
}

export class BasicAuthenticationService {
  constructor(private message: string) {
  }
}
