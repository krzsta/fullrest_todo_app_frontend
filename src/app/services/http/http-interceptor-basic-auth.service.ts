import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorBasicAuthService implements HttpInterceptor {

  constructor(private auth: AuthenticationService) {
  }

  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.auth.getAuthToken() && this.auth.getAuthUser()) {
      httpRequest = httpRequest
        .clone({
            setHeaders: {
              Authorization: this.auth.getAuthToken()
            }
          }
        );
    }
    return next.handle(httpRequest);
  }
}
