import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

export class HelloWorldMessage {
  constructor(public message: string) {
  }

}

@Injectable({
  providedIn: 'root'
})
export class WelcomeDataService {

  constructor(private http: HttpClient) {
  }

  executeHelloWorldRest() {
    return this.http.get<HelloWorldMessage>('http://localhost:8080/hello-world');
  }
}
